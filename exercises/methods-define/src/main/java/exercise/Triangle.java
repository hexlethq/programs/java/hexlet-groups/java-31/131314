package exercise;

class Triangle {
    // BEGIN
    public static void main(String[] args) {
        System.out.println(getSquare(4, 5, 45));
    }

    public static double getSquare(double side1, double side2, double angle) {
        return side1 * side2 / 2 * Math.sin(angle * Math.PI / 180);
    }
    // END
}
