package exercise;

class App {
    public static boolean isBigOdd(int number) {
        // BEGIN
        boolean isBigOddVariable = number >= 1001 && number % 2 != 0;
        // END
        return isBigOddVariable;
    }

    public static void sayEvenOrNot(int number) {
        // BEGIN
        if (number % 2 == 0) {
            System.out.println("yes");
        } else {
            System.out.println("no");
        }
        // END
    }

    public static void printPartOfHour(int minutes) {
        // BEGIN
        if (minutes < 15) {
            System.out.println("First");
            return;
        }
        if (minutes < 31) {
            System.out.println("Second");
            return;
        }
        if (minutes < 46) {
            System.out.println("Third");
            return;
        }

        System.out.println("Fourth");
        // END
    }
}
