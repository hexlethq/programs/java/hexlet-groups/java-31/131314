package exercise;

class App {
    // BEGIN
    public static void main(String[] args) {
        getFinalGrade(100, 12);
        assert getFinalGrade(100, 12) == 100 : "FAILED, should be 100"; // 100
        assert getFinalGrade(100, 12) == 100 : "FAILED, should be 100"; // 100
        assert getFinalGrade(99, 0) == 100 : "FAILED, should be 100"; // 100
        assert getFinalGrade(10, 15) == 100 : "FAILED, should be 100"; // 100
        assert getFinalGrade(85, 5) == 90 : "FAILED, should be 90"; // 90
        assert getFinalGrade(55, 3) == 75 : "FAILED, should be 75"; // 75
        assert getFinalGrade(55, 0) == 0 : "FAILED, should be 0"; // 0
    }

    public static String getTypeOfTriangle(int a1, int a2, int a3) {
        // All sides lengths should be > 0
        if (a1 < 1 || a2 < 1 || a3 < 1) {
            return "Треугольник не существует";
        }
        // Is triangle possible?
        if (a1 + a2 <= a3 || a1 + a3 <= a2 || a2 + a3 <= a1) {
            return "Треугольник не существует";
        }
        // Check the type of triangle
        if (a1 == a2 && a2 == a3) {
            return "Равносторонний";
        } else if (a1 == a2 || a1 == a3 || a2 == a3) {
            return "Равнобедренный";
        } else {
            return "Разносторонний";
        }
    }

    public static int getFinalGrade(int exam, int project) {
        if (exam > 90 || project > 10) {
            return 100;
        }
        if (exam > 75 && project > 5) {
            return 90;
        }
        if (exam > 50 && project > 2) {
            return 75;
        }
        return 0;
    }
    // END
}
