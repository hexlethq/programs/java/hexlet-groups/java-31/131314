package exercise;

class App {
    // BEGIN
    public static int[] sort(int[] arr) {
        if (arr.length == 0) {
            return arr;
        }
        // Чтоб не ходить до конца массива
        int lastIndex = arr.length - 1;
        for (int i = 0; i < arr.length; i++) {
            int changesCount = 0; //Если все отсортировано, выйдем
            for (int j = 0; j < lastIndex; j++) {
                if (arr[j] > arr[j + 1]) {
                    arr[j] += arr[j + 1];
                    arr[j + 1] = arr[j] - arr[j + 1];
                    arr[j] = arr[j] - arr[j + 1];
                    changesCount++;
                }
            }
            lastIndex--;
            if (changesCount == 0) {
                return arr;
            }
        }
        return arr; // I think it'll never executes
    }
    public static int[] selectionSort(int[] arr) {
        if (arr.length == 0) {
            return arr;
        }
        int lastIndex = arr.length - 1;
        for (int i = 0; i < arr.length; i++) {
            int changesCount = 0; //Если все отсортировано, выйдем
            int localMax = arr[0];
            int localMaxIndex = 0;
            for (int j = 0; j <= lastIndex; j++) {
                if (localMax < arr[j]) {
                    localMax = arr[j];
                    localMaxIndex = j;
                }
            }
            if (lastIndex != localMaxIndex) {
                arr[lastIndex] += arr[localMaxIndex];
                arr[localMaxIndex] = arr[lastIndex] - arr[localMaxIndex];
                arr[lastIndex] = arr[lastIndex] - arr[localMaxIndex];
                changesCount++;
            }
            if (changesCount == 0) {
                return arr;
            }
            lastIndex--;
        }
        return arr; // I think it'll never executes
    }
    // END
}
