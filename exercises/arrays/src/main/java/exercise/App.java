package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static void main(String[] args) {
        // Сие не работает - хочет инициализировать массив.
//        int[][] matrix1 = new int[][];
//        int[] result1 = App.flattenMatrix(matrix1);
//        System.out.println(Arrays.toString(result1));
// => []

        int[][] matrix2 = {
                {1, 2, 3},
                {4, 5, 6}
        };
        int[] result2 = App.flattenMatrix(matrix2);
        System.out.println(Arrays.toString(result2));
// => [1, 2, 3, 4, 5, 6]
    }
    public static int[] reverse(int[] numbers) {
        if (numbers.length == 0) {
            return numbers;
        }
        int[] result = new int[numbers.length];
        for (int i = numbers.length; i > 0; i--) {
            result[(numbers.length - i)] = numbers[i - 1];
        }
        return result;
    }
    public static int mult(int[] numbers) {
        int result = 1; // Потому что будем перемножать
        for (int number : numbers) {
            result *= number;
        }
        return result;
    }
    public static int[] flattenMatrix(int[][] matrix) {
        if (matrix.length == 0) {
            return new int[0];
        }
        int[] result = new int[matrix.length * matrix[0].length];
        int index = 0;
        for (int[] subArrays : matrix) {
            for (int cell : subArrays) {
                result[index] = cell;
                index++;
            }
        }
        return result;
    }
    // END
}
