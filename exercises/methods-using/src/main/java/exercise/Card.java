package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        String stars = "*".repeat(starsCount);
        cardNumber = stars + cardNumber.substring(12);
        System.out.println(cardNumber);
        // END
    }
}
