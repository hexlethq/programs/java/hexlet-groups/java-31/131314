package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static void main(String[] args) {
        int[] numbers1 = {5, 4, 34, 8, 11, -5, 1};
        System.out.println(App.getSumBeforeMinAndMax(numbers1)); // 19

        int[] numbers2 = {7, 1, 37, -5, 11, 8, 1};
        System.out.println(App.getSumBeforeMinAndMax(numbers2)); // 0
    }
    public static int getIndexOfMaxNegative(int[] arr) {
        int index = -1;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] < 0) {
                if (index == -1) {
                    index = i;
                }
                index = arr[i] > arr[index] ? i : index;
            }
        }
        return index;
    }

    public static int[] getElementsLessAverage(int[] arr) {
        if (arr.length == 0) {
            return arr;
        }
        int average = getArrayAverage(arr);
        int counter = 0;
        for (int i : arr) {
            if (i <= average) {
                counter++;
            }
        }
        int[] result = new int[counter];
        int index = 0;
        for (int i : arr) {
            if (i <= average) {
                result[index] = i;
                index++;
            }
        }
        return result;
    }

    private static int getArrayAverage(int[] arr) {
        int sum = 0;
        for (int i : arr) {
            sum += i;
        }
        return (sum / arr.length);
    }

    public static int getSumBeforeMinAndMax(int[] arr) {
        int min = Integer.MAX_VALUE;
        int indexMin = 0;
        int max = Integer.MIN_VALUE;
        int indexMax = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
                indexMax = i;
            }
            if (arr[i] < min) {
                min = arr[i];
                indexMin = i;
            }
        }
        //На случай, если все элементы одинаковые
        if (indexMax == indexMin) {
            return 0;
        }
        int[] tmpArr;
        if (indexMax > indexMin) {
            tmpArr = Arrays.copyOfRange(arr, indexMin + 1, indexMax);
        } else {
            tmpArr = Arrays.copyOfRange(arr, indexMax + 1, indexMin);
        }
        int sum = 0;
        for (int i : tmpArr) {
            sum += i;
        }
        return sum;
    }
    // END
}
