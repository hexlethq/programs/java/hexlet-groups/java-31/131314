package exercise;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Locale;

class App {
    // BEGIN
    public static String buildList(String[] strings) {
        if (strings.length == 0) {
            return "";
        }
        StringBuilder html = new StringBuilder();
        html.append("<ul>\n");
        for (String str : strings) {
            html.append("  <li>");
            html.append(str);
            html.append("</li>\n");
        }
        html.append("</ul>");
        return html.toString();
    }

    public static String getUsersByYear(String[][] users, int year) {
        StringBuilder html = new StringBuilder();
        html.append("<ul>\n");
        int counter = 0;
        for (String[] user : users) {
            // Да, я понимаю, что тут надо типа обработать дату
            // Но строка она и есть строка, обработка ничем отличаться не будет
            String userYear = user[1].substring(0, 4);
            if (Integer.parseInt(userYear) == year) {
                html.append("  <li>");
                html.append(user[0]);
                html.append("</li>\n");
                counter++;
            }
        }
        html.append("</ul>");
        return counter > 0 ? html.toString() : "";
    }
    // END

    // Это дополнительная задача, которая выполняется по желанию.
    public static String getYoungestUser(String[][] users, String date) throws Exception {
        // BEGIN
        // Без установки Locale не парсит буковки.
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy", Locale.ROOT);
        long millisToDate = sdf.parse(date).getTime();
        String nameYoungest = "";
        long millisYoungest = Long.MIN_VALUE;
        sdf = new SimpleDateFormat("yyyy-MM-dd");
        for (String[] user : users) {
            long millisUser = sdf.parse(user[1]).getTime();
            if (millisUser < millisToDate && millisUser > millisYoungest) {
                millisYoungest = sdf.parse(user[1]).getTime();
                nameYoungest = user[0];
            }
        }
        return nameYoungest;
        // END
    }
}
