package exercise;

class Point {
    // BEGIN
    public static void main(String[] args) {
        int[] point = Point.makePoint(-3, -6);
        int[] symmetricalPoint = Point.getSymmetricalPointByX(point);
        Point.pointToString(symmetricalPoint);

        int[] point1 = Point.makePoint(0, 0);
        int[] point2 = Point.makePoint(3, 4);
        Point.calculateDistance(point1, point2); // 5
    }
    /* Объект без объекта, какая прелесть.
       Все вокруг статичное,
       все вокруг мое.
       Следуем bad practice мы
       Опыт - наше все. :( */
    public static int[] makePoint(int x, int y) {
        return new int[]{x, y};
    }
    public static int getX(int[] point) {
        return point[0];
    }
    public static int getY(int[] point) {
        return point[1];
    }
    public static String pointToString(int[] point) {
        return ("(" + point[0] + ", " + point[1] + ")");
    }
    public static int getQuadrant(int[] point) {
        if (point[0] > 0 && point[1] > 0) {
            return 1;
        }
        if (point[0] < 0 && point[1] > 0) {
            return 2;
        }
        if (point[0] < 0 && point[1] < 0) {
            return 3;
        }
        if (point[0] > 0 && point[1] < 0) {
            return 4;
        }
        return 0;
    }
    public static int[] getSymmetricalPointByX(int[] point) {
        point[1] = -point[1];
        return point;
    }
    public static int calculateDistance(int[] point1, int[] point2) {
        double result = Math.pow(point1[0] - point2[0], 2);
        result = result + Math.pow(point1[1] - point2[1], 2);
        result = Math.sqrt(result);
        return (int) result;
    }
    // END
}
