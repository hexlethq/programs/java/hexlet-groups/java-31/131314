package exercise;

class App {
    public static void main(String[] args) {
        System.out.println(getAbbreviation("Asd q we"));
    }
    // BEGIN
    public static String getAbbreviation(String str) {
        if (str.isEmpty()) {
            return str;
        }
        str = str.trim();
        String result = str.substring(0, 1).toUpperCase();
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == ' ' && str.charAt(i + 1) != ' ') {
                result += str.toUpperCase().charAt(i + 1);
            }
        }
        return result;
    }
    // END
}
